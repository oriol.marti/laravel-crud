@extends('alumnos.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left text-center">
                <h3>Laravel practica mery</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('alumnos.create') }}"> Crear alumno</a>
            </div><br>
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <span>{{ $message }}</span>
    </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>nombre</th>
            <th>años</th>
        </tr>
        @foreach ($alumnos as $alumno)
        <tr>
            <td>{{ $alumno->nombre }}</td>
            <td>{{ $alumno->años }}</td>
            <td><form action="{{ route('alumnos.destroy',$alumno->id) }}" method="POST">
            <a class="btn btn-info" href="{{ route('alumnos.show',$alumno->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('alumnos.edit',$alumno->id) }}">Edit</a>
            @csrf
            @method('DELETE')
            <button type="submit" onclick="return confirm('Quieres eliminar a un alumno!?¿')" class="btn btn-danger">Delete</button>
            </form></td>
        </tr>
        @endforeach
    </table>
{!! $alumnos->links() !!}
@endsection
